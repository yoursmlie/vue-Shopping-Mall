import Vue from 'vue'
import App from './App.vue'
import Router from 'vue-router'
import router from './router/router'
import store from './store/store'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import fastClick from 'fastclick'
import VueLazyLoad from 'vue-lazyload'
import './utils/Toast/Toast'
import 'swiper/dist/css/swiper.css'
import '@/assets/style/base.css'
import moment from 'moment'

fastClick.attach(document.body)
moment.locale('zh-cn');
Vue.use(VueAwesomeSwiper)
Vue.use(VueLazyLoad, {
  loading: 'http://img2.imgtn.bdimg.com/it/u=2311480385,1126053999&fm=26&gp=0.jpg'
})


Vue.prototype.$bus = new Vue()
Vue.config.productionTip = false

const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
