import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import { setStorge, getStorge, clearStorge } from '../utils/LocalStorge/localStorge'

const list = getStorge('carlist')

export default new Vuex.Store({
  state: {
    cartList: list || []
  },
  mutations: {
    AddcartListCounter(state, payload) {
      payload.count++
      setStorge('carlist', state.cartList)
    },
    updateCartList(state, payload) {
      payload.checked = true
      state.cartList.push(payload)
      setStorge('carlist', state.cartList)
    },
    deleteCartList(state) {
      state.cartList = []
      clearStorge('carlist')
    },
    noSelectedAll(state) {
      state.cartList.forEach(item => {
        item.checked = false
      })
      setStorge('carlist', state.cartList)
    },
    SelectedAll(state) {
      state.cartList.forEach(item => {
        item.checked = true
      })
      setStorge('carlist', state.cartList)
    }
  },
  actions: {
    addToCart({ commit, state }, payload) {
      let oldProduct = null;
      for(let item of state.cartList) {
        if(item.id === payload.id) {
          oldProduct = item
        }
      }
      if(oldProduct) {
        commit('AddcartListCounter', oldProduct)
      }else {
        payload.count = 1
        return commit('updateCartList', payload)
      }
    },
    DeleteCart({commit}) {
      commit('deleteCartList')
    },
    noSelected({commit}) {
      commit('noSelectedAll')
    },
    selected({commit}) {
      commit('SelectedAll')
    }
  }
})
