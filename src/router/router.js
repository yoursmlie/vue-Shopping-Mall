import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
    path: '/home',
    component: () => import('../views/Home/Home.vue')
  },{
    path: '/cart',
    component: () => import('../views/Cart/Cart.vue')
  },{
    path: '/profile',
    component: () => import('../views/Profile/Profile.vue')
  },{
    path: '/detail/:id',
    component: () => import('../views/Deatil/Deatil.vue')
  },{
    path: '*',
    redirect: '/home'
  }]
})
