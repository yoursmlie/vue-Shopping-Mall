export const iconList = [
    {
        "id": "0001",
        "imgUrl": "https://s10.mogucdn.com/mlcdn/c45406/190702_5gc489a8e9ihjc75id9855ih7e7aa_150x150.jpg_640x640.v1cAC.40.webp",
        "desc": "女装"
    },
    {
        "id": "0002",
        "imgUrl": "https://s10.mogucdn.com/mlcdn/c45406/190702_7606lfhjd0d65bi663996c7i7cd7i_150x150.jpg_640x640.v1cAC.40.webp",
        "desc": "上衣"
    },
    {
        "id": "0003",
        "imgUrl": "https://s10.mogucdn.com/mlcdn/c45406/190702_0edi982h99ca9dk254h8bckdil0gf_150x150.jpg_640x640.v1cAC.40.webp",
        "desc": "裙子"
    },
    {
        "id": "0004",
        "imgUrl": "https://s10.mogucdn.com/mlcdn/c45406/190826_50f094ga735b68j2eel8jh3f6e6l7_150x150.jpg_640x640.v1cAC.40.webp",
        "desc": "女鞋"
    },
    {
        "id": "0005",
        "imgUrl": "https://s10.mogucdn.com/mlcdn/c45406/190627_6il8cab37b6f2k66ebh0lj90027f6_150x150.jpg_640x640.v1cAC.40.webp",
        "desc": "配饰"
    },
    {
        "id": "0006",
        "imgUrl": "https://s10.mogucdn.com/mlcdn/c45406/190702_326ihcfk2g36d309ihlljebk30e68_150x150.jpg_640x640.v1cAC.40.webp",
        "desc": "套装"
    },
    {
        "id": "0007",
        "imgUrl": "https://s10.mogucdn.com/mlcdn/c45406/190627_453h1450k9j52k5fl1l1d33c40j5a_150x150.jpg_640x640.v1cAC.40.webp",
        "desc": "美妆"
    },
    {
        "id": "0008",
        "imgUrl": "https://s10.mogucdn.com/mlcdn/c45406/190627_44ici7579i04k67596ifed5e3a3jc_150x150.jpg_640x640.v1cAC.40.webp",
        "desc": "裤子"
    }
]