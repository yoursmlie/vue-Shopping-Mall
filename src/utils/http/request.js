import axios from 'axios';

export const request = (config) => {
    const httpRequest = axios.create({
        baseURL: 'http://123.207.32.32:8000/api/v1',
        timeout: 5000
    })

    httpRequest.interceptors.response.use(res => {
        return res.data
    }, err => {
        console.log(err)
    })

    return httpRequest(config)
}