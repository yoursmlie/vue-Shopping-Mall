import Vue from 'vue'
import CreateAPI from 'vue-create-api'
import Toast from '../../components/content/Toast/Toast.vue'

Vue.use(CreateAPI)

Vue.createAPI(Toast, true)

Vue.mixin({
    methods: {
        toast(settings) {
            return this.$createToast({
                $props: {
                    settings 
                }
            })
        }
    }
})
