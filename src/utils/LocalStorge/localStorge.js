export const setStorge = (key, value) => {
    let list = JSON.stringify(value)
    localStorage.setItem(key, list)
}

export const getStorge = (key) => {
    let list =  localStorage.getItem(key)
    return JSON.parse(list)
}

export const clearStorge = (key) => {
    localStorage.removeItem(key)
}