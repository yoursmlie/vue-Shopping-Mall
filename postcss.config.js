module.exports = {
  plugins: {
    autoprefixer: {},
    'postcss-px-to-viewport': {
      viewportWidth: 375,   // 视窗的宽度
      viewportHeight: 1334, // 视窗的高度
      unitPrecision: 3,     // 指定`px`转换为视窗单位值的小数位数
      viewportUnit: "vw",   //指定需要转换成的视窗单位
      selectorBlackList: ['.ignore'],// 指定不转换为视窗单位的类
      minPixelValue: 1,     // 小于或等于`1px`不转换为视窗单位
      mediaQuery: false     // 允许在媒体查询中转换`px`
    }
  }
}
